
$(window).scroll(function() {

	var scroll = $(window).scrollTop();

	if (scroll >= 100) {

		$(".header-home-2, .header-home-1, .header-home-4 ").addClass("header-scrolled");
		$(".navbar").addClass("navbar-scrolled");
		$(".menu-2").addClass("menu-2-scrolled");
		$(".menu-1").addClass("menu-1-scrolled");



	} else {

		$(".header-home-2, .header-home-1").removeClass("header-scrolled");
		$(".navbar").removeClass("navbar-scrolled");
		$(".menu-2").removeClass("menu-2-scrolled");
		$(".menu-1").removeClass("menu-1-scrolled");
	}

});
function openNav() {
	document.getElementById("mySidenav").style.width = "330px";

}

function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
}




$(document).ready(function() {



	if ($(window).width() > 768) {
	}
	else {


	}
	$( ".thm-form .form-control" ).each(function( index ) {

		if($(this).val()=='')
		{
			$(this).closest(".field").removeClass("active");
		}else{
			$(this).closest(".field").addClass("active");
		}
	});



	$(".thm-form .form-control").focus(function(event){
		if($(this).val()=='')
		{
			$(this).closest(".field").addClass("active");

		}else{

			$(this).closest(".field").addClass("active");
		}


	});

	$(".thm-form .form-control").focusout(function(event){
		if($(this).val() !='')
		{
			$(this).closest(".field").addClass("active");
		}else{
			$(this).closest(".field").removeClass("active");
		}



	});


	if ($('#back-to-top').length) {
		var scrollTrigger = 100, // px
			backToTop = function () {
				var scrollTop = $(window).scrollTop();
				if (scrollTop > scrollTrigger) {
					$('#back-to-top').addClass('show');
				} else {
					$('#back-to-top').removeClass('show');
				}
			};
		backToTop();
		$(window).on('scroll', function () {
			backToTop();
		});
		$('#back-to-top').on('click', function (e) {
			e.preventDefault();
			$('html,body').animate({
				scrollTop: 0
			}, 700);
		});
	}



	var wow = new WOW({
		animateClass: 'animated',
		offset:100,
		callback:function(box) {}
	});
	wow.init();





	$(".col-toggler-2").click(function(){
		$(".menu-2").toggleClass("nav-open")
		$(".menu-1").toggleClass("nav-open")
	});


	$(".searchclick").click(function(){
		$(".nav-search-form").toggleClass("search-open");

		$(".searchclick").children(".ti-").toggleClass("svg-search").toggleClass("ti-close");


	});






	$(".login_but").click(function(){
		$(".nav-search-form").toggleClass("search-open");


		$(".searchclick").children(".ti-").toggleClass("svg-search").toggleClass("ti-close");


	});
	$('a[href*="#"]')
		.not('[href="#"]')
		.not('[href="#0"]')
		.not('.no_scroll_f')
		.click(function(event) {
			if (
				location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
				&&
				location.hostname == this.hostname
			) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

				if (target.length) {

					event.preventDefault();
					$('html, body').animate({
						scrollTop: target.offset().top
					}, 1000, function() {

						var $target = $(target);
						$target.focus();
						if ($target.is(":focus")) {
							return false;
						} else {
							$target.attr('tabindex','-1');
							$target.focus();
						};
					});
				}
			}
		});





	var owl = $('.owl-carousel-2');
	owl.owlCarousel({
		loop: true,
		nav: true,
		rtl: false,
		autoplay:false,
		margin: 15,
		autoplayTimeout:2000,
		smartSpeed: 3000,
		responsive: {
			0: {
				items: 2
			},
			600: {
				items: 3
			},
			960: {
				items: 4
			},
			1200: {
				items: 5
			}
		}
	});
	var owl = $('.owl-carousel-3');
	owl.owlCarousel({
		center:true,
		loop: true,
		items:4,
		nav: true,
		navText: [ '<img class="arrow-left " src="assets/images/a.svg">','<img class="arrow-right " src="assets/images/a.svg">' ],
		autoplay:false,
		margin: 0,

		smartSpeed: 800,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 2
			},
			960: {
				items: 2
			},
			1200: {
				items: 4
			}
		}
	});


	var owl = $('.charter-carousel');
	owl.owlCarousel({
		loop: true,
		nav: true,
		center:true,
		rtl: false,
		autoplay:false,
		navText: [ '<img class="arrow-left " src="assets/images/a.svg">','<img class="arrow-right " src="assets/images/a.svg">' ],
		margin: 15,
		dotsData: true,
		autoplayTimeout:2000,
		smartSpeed: 900,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 2
			},
			960: {
				items: 3
			},
			1200: {
				items: 4
			}
		}
	});

	var owl = $('.charter-carousel-2');
	owl.owlCarousel({
		loop: true,
		nav: true,
		center:true,
		rtl: false,
		autoplay:false,
		navText: [ '<img class="arrow-left " src="assets/images/a.svg">','<img class="arrow-right " src="assets/images/a.svg">' ],
		margin: 15,
		/*dotsData: true,*/
		autoplayTimeout:2000,
		smartSpeed: 900,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 2
			},
			960: {
				items: 2
			},
			1200: {
				items: 2
			}
		}
	});





	var owl = $('.testimonail_slider');
	owl.owlCarousel({
		loop: true,
		nav: false,
		center:false,
		rtl: false,
		autoplay:false,
		margin: 15,
		autoplayTimeout:2000,
		smartSpeed: 900,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			960: {
				items: 1
			},
			1200: {
				items: 1
			}
		}
	});

	var owl = $('.header-home-3');
	owl.owlCarousel({
		loop: true,
		nav: false,
		center:false,
		rtl: false,
		autoplay:true,
		margin: 0,
		animateOut: 'fadeOut',

		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			960: {
				items: 1
			},
			1200: {
				items: 1
			}
		}
	});


	var owl = $('.single-yacht');
	owl.owlCarousel({
		loop: true,
		nav: true,
		center:false,
		rtl: false,
		autoplay:true,
		navText: [ '<img class="arrow-left " src="assets/images/a.svg">','<img class="arrow-right " src="assets/images/a.svg">' ],
		margin: 0,
		animateOut: 'fadeOut',

		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			960: {
				items: 1
			},
			1200: {
				items: 1
			}
		}
	});




	$('.my-paroller').paroller();
	$('.jumbotron').paroller();
	$('.my-element').paroller();
	$('.thm-h-3-big').paroller();



	$(".header-home").mousemove(function(event){
		var compass_inner =$(".compass_inner");
		var x =(compass_inner.offset().left) + (compass_inner.width() / 2);
		var y =(compass_inner.offset().top) +(compass_inner.height() / 2);
		var rad = Math.atan2(event.pageX - x, event.pageY - y);
		var rot = (rad * (180 / Math.PI) * -1) + 180;
		compass_inner.css({
			'-webkit-transform': 'rotate(' + rot + 'deg)',
			'-moz-transform': 'rotate(' + rot + 'deg)',
			'-ms-transform': 'rotate(' + rot + 'deg)',
			'transform': 'rotate(' + rot + 'deg)'
		});
	});

	$('.main-tilt').tilt({
		maxTilt:        -15,
		perspective:    1000,

		scale:          1,
		speed:          100,
		transition:     true,
		disableAxis:    null,
		reset:          true,
		glare:          true,
		maxGlare:       0.5
	});












});



