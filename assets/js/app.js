$(document).ready(function ()
{
	$('.form-submit').submit(function (e) {
		e.preventDefault();


		$('.container-loading').hide().removeClass('hidden').fadeIn();

		$.ajax({
			url: $(this).attr('action'),
			type: $(this).attr('method'),
			data: $(this).serialize(),
			success: function () {

				$('.container-loading').fadeOut().addClass('hidden');

				Swal.fire({
					title: 'Pemesanan Kelas Berhasil',
					text: "Terima Kasih sudah melakukan pemesanan, Mohon menunggu untuk Follow Up dari Kami.",
					icon: 'success',
					showCancelButton: false,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Okay'
				}).then(function(result) {
					if (result.value) {
						window.location.reload();
					}
				});


			}
		});

		return false;
	});

	var $gallery = new SimpleLightbox('.gallery a', {});

});
