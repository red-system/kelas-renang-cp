<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak_email extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_kontak_email');
		$this->load->library('main');
		$this->load->helper('text');
		$this->main->check_admin();
	}

	public function index()
	{
		$data = $this->main->data_main();
		$data['kontak'] = $this->m_kontak_email->get_data()->result();
		$this->template->set('kontak', 'kt-menu__item--active');
		$this->template->set('breadcrumb', 'Management Kontak Email');
		$this->template->load_admin('kontak_email/index', $data);
	}


}
