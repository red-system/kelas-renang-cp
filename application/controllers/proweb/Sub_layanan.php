<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_layanan extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model(array('m_sub_layanan', 'm_category','m_layanan'));
		$this->load->library('main');
		$this->main->check_admin();
	}

	public function index($id)
	{
		$data = $this->main->data_main();
		$data['sub_layanan'] = $this->db
			->select('t.*, c.title AS category_title')
            ->where('t.id_layanan', $id)
			->where('t.id_language', $data['id_language'])
			->join('layanan c', 'c.id = t.id_layanan', 'left')
			->get('sub_layanan t')
			->result();
		$data['category'] = $this->m_category->get_where(array('use' => 'yes', 'id_language' => $data['id_language']))->result();
        $data['layanan'] = $this->db
            ->select('id,title')
            ->where('id', $id)
            ->get('layanan')
            ->row();

        $this->template->set('layanan', 'kt-menu__item--active');
		$this->template->set('breadcrumb', 'management layanan '.$data['layanan']->title);
		$this->template->load_admin('layanan/sub_layanan/index', $data);
	}

	public function createprocess()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Service Title', 'required');
//		$this->form_validation->set_rules('thumbnail_alt', 'Thumbnail Alternative', 'required');
		$this->form_validation->set_rules('meta_title', 'Meta title', 'required');
		$this->form_validation->set_rules('meta_description', 'Meta description', 'required');
		$this->form_validation->set_rules('meta_keywords', 'meta_keywords', 'required');

		$this->form_validation->set_error_delimiters('', '');
		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'title' => form_error('title'),
					'thumbnail_alt' => form_error('thumbnail_alt'),
					'description' => form_error('description'),
					'meta_title' => form_error('meta_title'),
					'meta_description' => form_error('meta_description'),
					'meta_keywords' => form_error('meta_keywords'),
				)
			));
		} else {

			$data = $this->input->post(NULL, TRUE);

			if ($_FILES['thumbnail']['name']) {
				$response = $this->main->upload_file_thumbnail('thumbnail', $this->input->post('title'));
				if (!$response['status']) {
					echo json_encode(array(
						'status' => 'error',
						'message' => 'Isi form belum benar',
						'errors' => array(
							'thumbnail' => $response['message']
						)
					));
					exit;
				} else {
					$data['thumbnail'] = $response['filename'];
				}
			}

			$data['id_language'] = $this->input->post('id_language');
			$data['id_layanan'] = $this->input->post('id_layanan');
			$data['use'] = $this->input->post('use');
			$data['title'] = $this->input->post('title');
			$data['url_title'] = url_title($this->input->post('title'));
			$data['title_sub'] = $this->input->post('title_sub');
			$data['thumbnail_alt'] = $this->input->post('thumbnail_alt');
			$data['description'] = $this->input->post('description');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['meta_keywords'] = $this->input->post('meta_keywords');
			$data['thumbnail'] = $response['filename'];

			$this->m_sub_layanan->input_data($data);

			echo json_encode(array(
				'status' => 'success',
				'message' => 'data berhasil diinput',
			));
		}
	}

	public function delete($id)
	{
		$where = array('id' => $id);
		$_id = $this->db->get_where('sub_layanan', $where)->row();
		$this->m_sub_layanan->delete_data($where, 'sub_layanan');
		unlink("upload/" . $_id->image);
	}

	public function update()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Service Title', 'required');
//		$this->form_validation->set_rules('thumbnail_alt', 'Thumbnail Alternative', 'required');
		$this->form_validation->set_rules('meta_title', 'Meta title', 'required');
		$this->form_validation->set_rules('meta_description', 'Meta description', 'required');
		$this->form_validation->set_rules('meta_keywords', 'meta_keywords', 'required');
		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'title' => form_error('title'),
					'thumbnail_alt' => form_error('thumbnail_alt'),
					'description' => form_error('description'),
					'meta_title' => form_error('meta_title'),
					'meta_description' => form_error('meta_description'),
					'meta_keywords' => form_error('meta_keywords'),
				)
			));
		} else {
			$id = $this->input->post('id');
			$data = $this->input->post(NULL, TRUE);
			$where = array(
				'id' => $id
			);

			if ($_FILES['thumbnail']['name']) {
				$response = $this->main->upload_file_thumbnail('thumbnail', $this->input->post('title'));
				if (!$response['status']) {
					echo json_encode(array(
						'status' => 'error',
						'message' => 'Isi form belum benar',
						'errors' => array(
							'thumbnail' => $response['message']
						)
					));
					exit;
				} else {
					$data['thumbnail'] = $response['filename'];
				}
			}
            $data['use'] = $this->input->post('use');
			$data['title'] = $this->input->post('title');
			$data['url_title'] = url_title($this->input->post('title'));
			$data['title_sub'] = $this->input->post('title_sub');
			$data['thumbnail_alt'] = $this->input->post('thumbnail_alt');
			$data['description'] = $this->input->post('description');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['meta_keywords'] = $this->input->post('meta_keywords');
			$data['thumbnail'] = $response['filename'];

			$this->m_sub_layanan->update_data($where, $data);


			echo json_encode(array(
				'status' => 'success',
				'message' => 'data berhasil diperbarui',
			));

		}
	}

}