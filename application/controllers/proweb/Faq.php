<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class faq extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_faq');
		$this->load->library('main');
		$this->main->check_admin();
	}

	public function index()
	{
		$data = $this->main->data_main();
		$data['faq'] = $this->db
			->select('t.*')
			->where('t.category', 'faq')
			->get('layanan t')
			->result();
		$this->template->set('faq', 'kt-menu__item--active');
		$this->template->set('breadcrumb', 'Management FAQ');
		$this->template->load_admin('faq/index', $data);
	}

	public function createprocess()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required|is_unique[blog_category.title]');
		$this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('display_at_home', 'Display at home', 'required');

		$this->load->model('m_faq');

		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'title' => form_error('title'),
					'description' => form_error('description'),

				)
			));
		} else {

			$data = $this->input->post(NULL);

			$data['category'] = 'faq';
            $this->m_faq->input_data($data);

			echo json_encode(array(
				'status' => 'success',
				'message' => 'data berhasil diinput',
			));
		}
	}

	public function delete($id)
	{
		$where = array('id' => $id);
//		$row = $this->m_faq->row_data($where);
//		$this->main->delete_file($row->thumbnail);
		$this->m_faq->delete_data($where);
	}

	public function update()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required|is_unique[blog_category.title]');
		$this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('display_at_home', 'Display at home', 'required');
		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'title' => form_error('title'),

				)
			));
		} else {

			$id = $this->input->post('id');
			$data = $this->input->post(NULL);
			$where = array(
				'id' => $id
			);



			$this->m_faq->update_data($where, $data);
			echo json_encode(array(
				'status' => 'success',
				'message' => 'data berhasil diinput'
			));
		}
	}
}
