<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Count extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_count');
		$this->load->library('main');
		$this->main->check_admin();
	}

	public function index()
	{
		$data = $this->main->data_main();
		$data['count'] = $this->m_count->get_data()->result();
		$this->template->set('count', 'kt-menu__item--active');
		$this->template->set('breadcrumb', 'Management Count');
		$this->template->load_admin('count/index', $data);
	}

	public function createprocess()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('sub_title', 'Sub Title', 'required');
		$this->form_validation->set_rules('nilai', 'Nilai', 'required');
        $this->form_validation->set_error_delimiters('', '');

		$title = $this->input->post('title');

		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'title' => form_error('title'),
					'sub_title' => form_error('sub_title'),
					'nilai' => form_error('nilai'),
//					'thumbnail_alt' => form_error('thumbnail_alt'),
				)
			));
		} else {

			$data = $this->input->post(NULL, TRUE);

			$this->m_count->input_data($data);

			echo json_encode(array(
				'status' => 'success',
				'message' => 'data berhasil diinput',
			));
		}
	}

	public function delete($id)
	{

		$where = array('id' => $id);
		$this->m_count->delete_data($where);
	}

	public function update()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('sub_title', 'Sub Title', 'required');
		$this->form_validation->set_rules('nilai', 'Nilai', 'required');
		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'title' => form_error('title'),
					'sub_title' => form_error('sub_title'),
					'nilai' => form_error('nilai'),
				)
			));
		} else {

			$this->load->model('m_slider');
			if ($this->form_validation->run() === FALSE) {
				echo json_encode(array(
					'status' => 'error',
					'message' => 'Isi form belum benar2',
					'errors' => array(
						'title' => form_error('title'),
//						'description' => form_error('description'),
					)
				));
			} else {

				$id = $this->input->post('id');
				$data = $this->input->post(NULL, TRUE);
				$where = array(
					'id' => $id
				);



				$this->m_count->update_data($where, $data);
				echo json_encode(array(
					'status' => 'success',
					'message' => 'data berhasil diinputkan'
				));
			}
		}
	}
}
