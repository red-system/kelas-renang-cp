<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Controller

{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
	}



	public function index()
	{
		$data = $this->main->data_front();

		$data['meta'] = $this->db->where('type','profile')->get('pages')->row();
		$data['list_team'] = $this->db->get('team')->result();
		$data['gallery'] = $this->db->get('gallery')->result();

		$this->load->view('user/templates/header-compro',$data);
		$this->load->view('user/title-header');
		$this->load->view('user/team-detail');
		$this->load->view('user/enquire');
		$this->load->view('user/templates/footer-compro');
	}

}
