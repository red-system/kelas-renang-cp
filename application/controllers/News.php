<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
	}
	public function index()
	{
		$data =  $this->main->data_front();
		$this->load->library('pagination');
		$config['base_url'] = base_url().'news';

		$config['total_rows'] = $this->db->select('count(id) as qty')->where('use','yes')->get('news')->row()->qty;
		$config['per_page'] = 6;
		$config['use_page_numbers'] = TRUE;
		$config['uri_segment'] = 2;
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li><a class="y-active_page">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open']     = '<li>';
		$config['num_tag_close']    = '</li>';
		$this->pagination->initialize($config);
		$data['paging'] = $this->pagination->create_links();

		$page = $this->uri->segment(2) == '' ? 1 : $this->uri->segment(2);
		$start = ($page - 1) * $config['per_page'];
		$data['meta'] = $this->db->where('type','news')->get('pages')->row();
		$list = $this->db->get('news',$config['per_page'],$start)->result();
		foreach ($list as $item){
			$item->menu_url = base_url().'news/'.$this->main->slug($item->title);
		}
		$data['list'] = $list;
		$this->load->view('user/templates/header-compro',$data);
		$this->load->view('user/title-header');
		$this->load->view('user/news');
		$this->load->view('user/templates/footer-compro');
    }

	public function item($id=''){
		$data =  $this->main->data_front();

		$data['meta'] =  $this->db->where('id',$id)->get('news')->row();
		$this->load->view('user/templates/header-compro',$data);
		$this->load->view('user/title-header');
		$this->load->view('user/program_detail');
		$this->load->view('user/enquire');
		$this->load->view('user/templates/footer-compro');
	}
}
