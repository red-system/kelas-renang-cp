<?php

use PHPMailer\PHPMailer\PHPMailer;

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
	}

	public function index()
	{
		$slider = $this->db->where('use','yes')->get('slider')->result();
		$data =  $this->main->data_front();
		$data['slider'] = $slider;
		$page_image = $this->db->where('use','yes')->where('type','page_image')->get('gallery')->row();

		$data['page_image'] = $page_image;
		$data['about_page'] = $this->db->where('type','profile')->get('pages')->row();
		$data['service_page'] = $this->db->where('type','services')->get('pages')->row();
		$data['gallery_page'] = $this->db->where('type','gallery_photo')->get('pages')->row();
		$data['testimonial_page'] = $this->db->where('type','testimonial')->get('pages')->row();
		$data['services'] = $this->db->where('category','layanan')->get('layanan')->result();
		$data['testimonial'] = $this->db->where('use','yes')->get('comment')->result();
		$data['meta'] = $this->db->where('type','home')->get('pages')->row();
		$data['gallery'] = $this->db->where('use','yes')->where('type','gallery')->get('gallery')->result();
		$data['list_team'] = $this->db->where('use', 'yes')->get('team')->result();
		$this->load->view('user/templates/header-compro',$data);
		$this->load->view('user/slider-with-no-detail');
		$this->load->view('user/history');
		$this->load->view('user/services');
		$this->load->view('user/galery');
		$this->load->view('user/team');
		$this->load->view('user/testimonial');
		$this->load->view('user/templates/footer-compro');
	}

	public function thanks()
	{
		echo 'thanks';
	}

	public function mailchimp()
	{
		$email = $this->input->post('email');
		echo "simpan " . $email . " ke mailchimp";
	}

	public function sign_up()
	{
		echo 'proses kirim email';
	}

	public function faq()
	{
		$this->load->view('templates/header-with-book-no-menu');
		$this->load->view('faq');
		$this->load->view('templates/footer');
	}


	public function book()
	{
		$this->load->library('session');

		$first_name = $this->input->post('first_name');
		$last_name = $this->input->post('last_name');
		$email = $this->input->post('email');
		$kota = $this->input->post('kota');
		$whatsapp = $this->input->post('whatsapp');
		$services = $this->input->post('services');
		$message = $this->input->post('message');

		$data['first_name'] = $first_name;
		$data['last_name'] = $last_name;
		$data['email'] = $email;
		$data['whatsapp'] = $whatsapp;
		$data['services'] = $services;
		$data['messaage'] = $message;

		$this->session->set_userdata($data);

		require APPPATH . 'libraries/Exception.php';
		require APPPATH . 'libraries/PHPMailer.php';
		require APPPATH . 'libraries/SMTP.php';


		// PHPMailer object
		$response = false;
		$mail_user = new PHPMailer();
		$mail_admin = new PHPMailer();


		$host = 'mail.kelasrenang.id';
		$username = 'support@kelasrenang.id';
		$password = 'Support123!@#';

		// Email body content
		$mailContentAdmin =
			"Firsts Name : " . $first_name . "<br>" .
			"Last Name : " . $last_name . "<br>" .
			"Email : " . $email . "<br>" .
			"Kota : " . $kota . "<br>" .
			"WhatsApp : " . $whatsapp . "<br>" .
			"Services : " . $services . "<br>" .
			"Message : " . $message . "<br>" ;

			$mailContentUser = '
			<br>Hi, ' . $first_name . ' ' . $last_name . ',<br />
			Selamat datang di KELAS RENANG,<br />
			Terima kasih telah memilih kelasrenang untuk pengalaman renang anda. Kami akan segera menghubungi Anda untuk membantu anda dalam menentukan jadwal yang terbaik dan sesuai 😊<br />
			<Br />
			Warm regards,<br />
			Kelas Renang Administrator<br />
		';


		/**
		 * Send to User
		 */

		try {
			$mail_user->IsSMTP();
			$mail_user->SMTPSecure = "ssl";
			$mail_user->Host = $host; //hostname masing-masing provider email
			$mail_user->SMTPDebug = 2;
			$mail_user->SMTPDebug = FALSE;
			$mail_user->do_debug = 0;
			$mail_user->Port = 465;
			$mail_user->SMTPAuth = true;
			$mail_user->Username = $username; //user email
			$mail_user->Password = $password; //password email
			$mail_user->SetFrom("support@kelasrenang.id ", 'Kelas Renang'); //set email pengirim
			$mail_user->Subject = 'Book - Kelas Renang'; //subyek email
			$mail_user->AddAddress($email, $first_name . ' ' . $last_name); //tujuan email
			$mail_user->MsgHTML($mailContentUser);
//			if ($file) {
//				$mail->addAttachment("upload/images/" . $file);
//			}
			$mail_user->Send();
			//echo "Message has been sent";
		} catch (phpmailerException $e) {
			echo $e->errorMessage(); //Pretty error messages from PHPMailer
		} catch (\Exception $e) {
			echo $e->getMessage(); //Boring error messages from anything else!
		}

		/**
		 * Send to Admin
		 */
		try {
			$mail_admin->IsSMTP();
			$mail_admin->SMTPSecure = "ssl";
			$mail_admin->Host = $host;
			$mail_admin->SMTPDebug = 2;
			$mail_admin->SMTPDebug = FALSE;
			$mail_admin->do_debug = 0;
			$mail_admin->Port = 465;
			$mail_admin->SMTPAuth = true;
			$mail_admin->Username = $username; //user email
			$mail_admin->Password = $password; //password email
			$mail_admin->SetFrom("support@kelasrenang.id ", 'Kelas Renang'); //set email pengirim
			$mail_admin->Subject = 'Book - Kelas Renang'; //subyek email
			$mail_admin->AddAddress($username, 'Kelas Renang'); //tujuan email
			$mail_admin->MsgHTML($mailContentAdmin);

			$mail_admin->Send();

		} catch (phpmailerException $e) {
			echo $e->errorMessage();
		} catch (\Exception $e) {
			echo $e->getMessage();
		}


	}
}
