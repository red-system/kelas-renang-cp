<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
	}
	public function index()
	{
		$data =  $this->main->data_front();
		
		$data['meta'] = $this->db->where('type','location')->get('pages')->row();
		$list = $this->db->get('location')->result();
		$data['list'] = $list;
        $this->load->view('user/templates/header-compro',$data);
        $this->load->view('user/title-header');
		$this->load->view('user/location');
		$this->load->view('user/templates/footer-compro');
    }
    

// 	public function item($id=''){
// 		$data =  $this->main->data_front();

// 		$data['meta'] =  $this->db->where('id',$id)->get('location')->row();
// 		$this->load->view('user/templates/header-compro',$data);
// 		$this->load->view('user/title-header');
// 		$this->load->view('user/program_detail');
// 		$this->load->view('user/enquire');
// 		$this->load->view('user/templates/footer-compro');
// 	}
}
