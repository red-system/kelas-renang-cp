<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller
{

	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
	}
	public function index()
	{

		$data =  $this->main->data_front();
		$data['captcha'] = $this->main->captcha();
		$data['meta'] = $this->db->where('type','contact_us')->get('pages')->row();
        $this->load->view('user/templates/header-compro',$data);
        $this->load->view('user/title-header');
		$this->load->view('booking-form');
		$this->load->view('user/templates/footer-compro');
	}
	public function captcha_check($str)
	{
		if ($str == $this->session->userdata('captcha_mwz')) {
			return TRUE;
		} else {
			$this->form_validation->set_message('captcha_check', 'security code was wrong, please fill again truly');
			return FALSE;
		}
	}
	function send_message(){
		error_reporting(0);
		$this->load->model('m_kontak_email');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('whatsapp', 'Whatsap', 'trim|required');
		$this->form_validation->set_rules('kota', 'Kota', 'trim|required');
		$this->form_validation->set_rules('services', 'Programs', 'trim|required');
		$this->form_validation->set_rules('message', 'Message', 'trim|required');
		$this->form_validation->set_rules('captcha', 'Security Code', 'trim|required|callback_captcha_check');
		$this->form_validation->set_error_delimiters('<span class="error-message" style="color:red">', '</span>');
		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Fill form completly',
				'errors' => array(
					'first_name' => form_error('first_name'),
					'last_name' => form_error('last_name'),
					'whatsapp' => form_error('whatsapp'),
					'kota' => form_error('kota'),
					'services' => form_error('services'),
					'email' => form_error('email'),
					'message' => form_error('message'),
					'captcha' => form_error('captcha'),
				)
			));
			return false;
		} else{
			$email_admin = $this->db->where('use', 'yes')->get('email')->result();
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$whatsapp = $this->input->post('whatsapp');
			$kota = $this->input->post('kota');
			$services = $this->input->post('services');
			$email = $this->input->post('email');
			$message = $this->input->post('message');
			$data = array(
				'first_name' => $first_name,
				'last_name' => $last_name,
				'phone' => $whatsapp,
				'kota' => $kota,
				'services' => $services,
				'message' => $message,
			);
			$this->m_kontak_email->input_data($data);
			$mailContentAdmin =
				"Firsts Name : " . $first_name . "<br>" .
				"Last Name : " . $last_name . "<br>" .
				"Email : " . $email . "<br>" .
				"Kota : " . $kota . "<br>" .
				"WhatsApp : " . $whatsapp . "<br>" .
				"Services : " . $services . "<br>" .
				"Message : " . $message . "<br>" ;
			foreach($email_admin as $r) {
				$this->main->mailer_auth('Kontak Kami - Website', $r->email, $this->main->web_name().' Administrator ', $mailContentAdmin);
			}
			echo json_encode(array(
				'status'=>'success',
				'message'=>'Email as already sent, please wait a moment for our reply ^_^'
			));

		}

	}

}
