<section id="y-single_info">
	<div class="y-single_info">
		<div class="container-fluid">
			<div class=" y-single_info_inner y-section_content">
				<div class="event-container row">
					<?php
						$i = 1;
						foreach ($list as $item){
							?>
							<div class="col-xs-12 col-md-12 col-lg-4 row-event">
								<div class="y-yacht_intro_img col-xs-12 col-md-6 col-lg-12 padding-event">
									<a href="<?=$item->menu_url?>" title="<?=$item->title?>"><img src="<?=site_url().'upload/images/'.$item->thumbnail?>" class="img-fluid" title="<?=$item->title?>" alt="<?=$item->thumbnail_alt?>"></a>
								</div>
								<div class="y-yacht_intro col-xs-12 col-md-6 col-lg-12 content-event">
									<a href="<?=$item->menu_url?>" title="<?=$item->title?>"><?=$item->title?></a>

									<span class="truncate-span">
										<?=$item->description?>
									</span>
									<a href="<?=$item->menu_url?>" title="<?=$item->title?>" class="read-more button-fancy -color-1"><span class="btn-arrow"></span><span class="twp-read-more text">Read More</span></a>
								</div>
							</div>
							<?php
							if(($i%3)==0){
								?>
							</div>
								<div class="event-container row">
								<?php
							}

							$i++;

						}
					?>
				</div>
				<div class="y-pagination row clearfix">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
						<ul class="row y-pagination_num clearfix">
							<?=$paging?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
