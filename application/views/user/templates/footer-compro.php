</main>
<footer class="footerdark footer-1 swip">
	<section id="h-newslatter" class="h-newslatter main-section no-a-b ">
		<div class="container-fluid">
			<div class="row m-0">
				<div class="col-md-3">
					<div class="footer-box">
						<h2 class="wow fadeInUp" data-wow-delay="1s">Contact Us</h2>
						<div class="textwidget">
							<p>Perumahan Griya Alam, Pecatu, Kec. Kuta Sel., Kabupaten Badung, Bali 80361, Indonesia
							</p>
							<p><a href="tel:+628990654435">+628990654435</a><br>
								<a href="mailto:info@kelasrenang.id" title="contact kelas renang">info@kelasrenang.id</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="footer-box">
						<h2 class="wow fadeInUp" data-wow-delay="1s">Need Help?</h2>
						<div class="textwidget">
							<ul>
								<li>
									<a href="<?php echo base_url(); ?>contact" title="kelas renang contact">Contact</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="footer-box">
						<h2 class="wow fadeInUp" data-wow-delay="1s">Tags</h2>
						<div class="textwidget">
							<ul class="tagnav">
								<li>
									<a href="#">Swimming Class</a>
								</li>
								<li>
									<a href="#">Best Swimming Coach</a>
								</li>
								<li>
									<a href="#">Certified Coach</a>
								</li>
								<li>
									<a href="#">Complete Swimming Program</a>
								</li>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="h-newslatter-inner footer-box">
						<div class="thm-h  sec-main-h">
							<h2 class="wow fadeInUp" data-wow-delay="1s">DISCOVER OUR WORLD</h2>
						</div>
						<?php echo form_open_multipart('home/mailchimp	', 'class="form-inline  text-center justify-content-center thm-form wow fadeInUp"  data-wow-duration="1s"'); ?>
						<div class="field">
							<label>Enter your email</label>
							<input class="form-control " type="search" id="email" name="email" aria-label="Search">
						</div>
						<div class="submit_news">
							<input type="submit" value="">
							<span class="submit-arrow">
                                 <img src="<?php echo base_url(); ?>assets/images/ap.svg">
                                 </span>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="" class="footermain main-section dark no-a-b pb-1 pt-1 ">
		<div class="container-fluid">
			<div class="row m-0">
				<div class="col-sm-12">
					<div class="row mb-3 mt-3">
						<ul class="social p-0 col-sm-4 text-center ">

						</ul>
						<ul class="social p-0 col-sm-4 text-center mt-3">
							<li>
								<a href="https://www.facebook.com/kelasrenang.id" title="facebook kelas renang" target="_blank">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<li>
 								<a href="https://www.instagram.com/kelasrenang.id/" title="instagram kelas renang" target="_blank">
									<i class="fab fa-instagram"></i>
								</a>
							</li>
						</ul>
						<ul class="social p-0 col-sm-4 text-center mt-3">

						</ul>
					</div>
					<div class="footer-logo mt-3 mb-3">
						<a class="navbar-brand text-center p1" href="<?php echo base_url(); ?>">
							<img src="<?php echo base_url(); ?>assets/images/logo-kelas-renang-putih-5x5.png" alt="kelas-renang" title="kelas-renang">
						</a>
					</div>
				</div>
				<div class="col-sm-12 copysec">
					<p>© Copyright 2020 by <a title="Reserved by RED SYSTEM" href="https://redsystem.id/">RedSystem</a>. All rights reserved.</p>
				</div>
			</div>
		</div>
	</section>
</footer>

<div class='container-loading hidden'>
	<div class='loader'>
		<div class='loader--dot'></div>
		<div class='loader--dot'></div>
		<div class='loader--dot'></div>
		<div class='loader--dot'></div>
		<div class='loader--dot'></div>
		<div class='loader--dot'></div>
		<div class='loader--text'></div>
		<div class='loader--desc'></div>
	</div>
</div>




<script src="<?php echo base_url(); ?>assets/js/default.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

</body>
</html>
