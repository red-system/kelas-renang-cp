<!DOCTYPE html>
<html lang="en">
<head>
	<title>Kelas Renang</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keyword" content="kelas renang, pelatihan renang, private renang">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/fav-icon-new.png">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Sen:400,700,800&amp;display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/themify-icons.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/all.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
	<link href="<?php echo base_url(); ?>assets/css/magicscroll.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/loading.css">
</head>
<body>
<a href="home-1.html#" id="back-to-top" title="Back to top">
	<img class="arrow-up "
		 src="<?php echo base_url(); ?>assets/images/a.svg"></a>
<div id="main-2">
	<header>
		<div class="header-home-2" style="background-image: url(<?php echo base_url(); ?>assets/images/cover-2.jpg)">
			<div class="container-fluid">
				<div class="header-cont">
					<h1 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">Sekolah Renang, One By One
						Teaching Method</h1>
					<!-- <h5 class="wow fadeInUp" data-wow-duration="1s" class="wow fadeInUp" >kami sangat mengutamakan kenyamanan pada saat belalar terutama untuk masyrakat yang mempunyai trauma ataupun fobia dengan air.</h5> -->
				</div>
			</div>
		</div>
		<div id="mainnavbar" class="container-fluid p-0 menu-2 ">
			<div class="fixed-top  container-fluid">
				<div class="row menu-1-inner justify-content-between example-center-div example-center-div-cover">
					<div class="">
						<a class="example-center-div" href="<?php echo base_url(); ?>">
							<img class="nb-light example-logo-image-size"
								 src="<?php echo base_url(); ?>assets/images/logo-kelas-renang-warna.png">
							<span></span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</header>
	<main>
		<section class="swip h2-book-section"
				 style="background-image: url(<?php echo base_url(); ?>assets/images/cover-3.jpg)">
			<div class="container-fluid section-menu ">
				<div class="row">
					<div class="col-md-5 home-about wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
						<div class="thm-h ">
							<h2 class="wow fadeInUp" data-wow-duration="0.7s">Pesan Sekarang!</h2>
							<h4>sekolah renang yang berbasiskan pengajaran one by one teaching (private)</h4>
						</div>
						<p>Kami sangat mengutamakan kenyamanan pada saat belajar terutama untuk masyrakat yang mempunyai
                            trauma ataupun fobia dengan air. <a href="https://www.kelasrenang.id"><i>kelasrenang.id</i></a> berdiri pada tahun 2015 di
							Jakarta. <br>Saat ini <a href="https://www.kelasrenang.id"><i>kelasrenang.id</i></a> telah mengepakan sayap ke Bali dan Solo sebagai langkah untuk mewujudkan visi menjadi lembaga pelatihan renang untuk Indonesia. <a href="https://www.kelasrenang.id"><i>kelasrenang.id</i></a> tidak hanya fokus terhadap pelatihan renang, akan tetapi selalu terus mengembangkan program program yang berhubungan dengan media air.
						</p>
					</div>
					<div class="col-md-3">
					</div>
					<div class="col-md-4  wow fadeInUp">
						<div class="home-booknow">
							<div class="booknow_box">
								<h4>Book now</h4>
								<?php echo form_open_multipart('home/book', 'class="form-submit"'); ?>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="First name" name="first_name"
										   required>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Last name" name="last_name"
										   required>
								</div>
								<div class="form-group">
									<input type="email" class="form-control" placeholder="Email" name="email" required>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Kota" name="kota" required>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Mobile Number (Whatsapp)"
										   name="whatsapp" required>
								</div>
								<div class="form-group">
									<select class="form-control" id="kelas_pilihan" name="services" required>
										<option value="Select Class Type">Select Class Type</option>
										<option value="Aqua Prenatal Yoga">Aqua Prenatal Yoga</option>
                                        <option value="Prenatal Swimming">Prenatal Swimming</option>
										<option value="Baby Class">Baby Class</option>
										<option value="Toddler Class">Toddler Class</option>
										<option value="Kid's Class">Kid's Class</option>
										<option value="Teen Class">Teen Class</option>
										<option value="Adult Class">Adult Class</option>
										<option value="Special Needs and Therapy Class">Special Needs and Therapy Class</option>
                                        <option value="Intensive Class">Intensive Class</option>
									</select>
								</div>
								<div class="form-group">
									<textarea class="form-control" placeholder="Message" name="message"
											  required></textarea>
								</div>
								<button type="submit" class="btn btn-primary">Kirim Pemesanan Kelas</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
 
            
