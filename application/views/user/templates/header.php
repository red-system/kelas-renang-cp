<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Luxury Yachts | Sales, Charter, Management &amp; Crew | YAWL</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/png" href="assets/images/fav-icon.png">
      <link rel="stylesheet" href="assets/css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Sen:400,700,800&amp;display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" href="assets/css/themify-icons.css">
      <link rel="stylesheet" href="assets/css/all.min.css">
      <link rel="stylesheet" href="assets/css/animate.css">
      <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
      <link href="assets/css/magicscroll.css" rel="stylesheet" type="text/css" media="screen"/>
      <link rel="stylesheet" href="assets/css/style.css">
   </head>
   <body >
      <a href="home-1.html#" id="back-to-top" title="Back to top"><img class="arrow-up " src="assets/images/a.svg"></a>
      <div id="main-2">
         <header>
            <div id="mainnavbar" class="container-fluid p-0 menu-2 "  >
               <div class=" fixed-top  container-fluid">
                  <div class="row menu-1-inner justify-content-between"  >
                     <div class="col-auto menu-1-logo">
                        <a class="navbar-brand" href="home-1.html">
                        <img class="nb-light" src="assets/images/logo-kelas-renang-putih.png">
                        <span>Kelas Renang Bali</span>
                        </a>
                     </div>
                     <div class="col-md-7 main_nav_outer">
                        <nav class="main_nav">
                           <div class="dropdown ">
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                              Home
                              </button>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="home-1.html">Homepage 1</a>
                                 <a class="dropdown-item" href="home-2.html">Homepage 2</a>
                                 <a class="dropdown-item" href="home-3.html">Homepage 3</a>
                              </div>
                           </div>
                           <div class="dropdown ">
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                              Sale
                              </button>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="sale_listing.html">Sale Listing</a>
                                 <a class="dropdown-item" href="sale_single.html">Sale Single</a>
                              </div>
                           </div>
                           <div class="dropdown ">
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                              Charter 
                              </button>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="charter_listing.html">Charter Listing</a>
                                 <a class="dropdown-item" href="charter_single.html">Charter Single</a>
                              </div>
                           </div>
                           <div class="dropdown ">
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                              Destination
                              </button>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="destination_listing.html">Destination Listing</a>
                                 <a class="dropdown-item" href="destination_single.html">Destination Single</a>
                              </div>
                           </div>
                           
                           <div class="dropdown ">
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                              Other Pages
                              </button>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="about.html">About Us</a>
                                 <a class="dropdown-item" href="404.html">404</a>
                              </div>
                           </div>
                        </nav>
                     </div>
                     <div class="col-auto nav-search">
                        <div class="login_but">
                            <a href="http://veepixel.com/tf/html/yawl/cart.html">
                               <i class="ti-shopping-cart"></i>
                                <span class="cart_items">
                                  <span class="cart_items_count">2</span>
                                </span>
                            </a>
                            <div class="cart_index">
                              <h5><a href="http://veepixel.com/tf/html/yawl/cart.html">My cart</a></h5>
                              <ul>
                                 <li>
                                    <span class="cart_porductname">Bikiht ciheyc</span>
                                    <span class="cart-price-amount">
                                        <span class="Price-currency">£</span>3.67M
                                    </span>
                                    <span class="cart_remove">
                                       <i class="ti-close"></i>
                                    </span>
                                 </li>
                                 <li>
                                    <span class="cart_porductname">Eledikiht leik</span>
                                    <span class="cart-price-amount">
                                        <span class="Price-currency">£</span>5.08M
                                    </span>
                                    <span class="cart_remove">
                                       <i class="ti-close"></i>
                                    </span>
                                 </li>
                              </ul>
                              <div class="cart_totals">
                                 <a class="checkout_btn" href="http://veepixel.com/tf/html/yawl/checkout.html">Order now</a>
                                 <span class="cart-price-amount">
                                    <span class="Price-currency">£</span>8.75M
                                 </span>
                              </div>
                            </div>
                         </div>
                        <button class="searchclick">
                          <span class="ti- ti-search  svg-search"></span>
                        </button>
                        <form class="nav-search-form">
                           <input class="form-control " type="search" placeholder="Search" aria-label="Search">
                           <button class=" btn btn-outline-success my-2 my-sm-0" type="submit"><i class="ti-search"></i></button>
                        </form>
                        <span class=" col-toggler-2">
                           <button class="navbar-toggler collapsed" type="button" onclick="openNav()" >
                              <div class="hamburger hamburger--spring js-hamburger">
                                 <div class="hamburger-box">
                                    <div class="hamburger-inner"></div>
                                 </div>
                              </div>
                           </button>
                        </span>
                     </div>
                  </div>
                  <div  id="mySidenav" class=" navbar-4 sidenav">
                     <nav class=" ">
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <div class=" ">
                           
                           <div class="sidenav_cat sc_first">
                              <h3>Social links</h3>
                              <ul class="tagnav">
                                 <li>
                                    <a href="home-1.html#"><span class=" ti-facebook "></span></a>
                                 </li>
                                 <li>
                                    <a href="home-1.html#"><span class=" ti-twitter-alt "></span></a>
                                 </li>
                                 <li>
                                    <a href="home-1.html#"><span class=" ti-pinterest "></span></a>
                                 </li>
                                 <li>
                                    <a href="home-1.html#"><span class=" ti-linkedin "></span></a>
                                 </li>
                              </ul>
                           </div>
                           <div class="sidenav_cat">
                              <h3>Menu</h3>
                              <ul class="">
                                 <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="home-1.html#" data-toggle="dropdown">Home</a>
                                    <div class="dropdown-menu">
                                       <a class="dropdown-item" href="home-1.html">Home-1</a>
                                       <a class="dropdown-item" href="home-2.html">Home-2</a>
                                       <a class="dropdown-item" href="home-3.html">Home-3</a>                      
                                    </div>
                                 </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="about.html">About</a>
                                 </li>
                                 <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="home-1.html#" data-toggle="dropdown">Destination</a>
                                    <div class="dropdown-menu">
                                       <a class="dropdown-item" href="destination_listing.html">Destination Listing</a>
                                       <a class="dropdown-item" href="destination_single.html">Destination Single</a>                      
                                    </div>
                                 </li>
                                 <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="home-1.html#" data-toggle="dropdown">For Charter</a>
                                    <div class="dropdown-menu">
                                       <a class="dropdown-item" href="charter_listing.html">Charter Listing</a>
                                       <a class="dropdown-item" href="charter_single.html">Charter Single</a>                      
                                    </div>
                                 </li>
                                 <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="home-1.html#" data-toggle="dropdown">Sale</a>
                                    <div class="dropdown-menu">
                                       <a class="dropdown-item" href="sale_listing.html">Sale Listing</a>
                                       <a class="dropdown-item" href="sale_single.html">Sale Single</a>                      
                                    </div>
                                 </li>
                                 <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="home-1.html#" data-toggle="dropdown">Blog</a>
                                    <div class="dropdown-menu">
                                       <a class="dropdown-item" href="post_listing.html">Post Listing</a>
                                       <a class="dropdown-item" href="post_single.html">Post Single</a>                
                                    </div>
                                 </li>
                                 <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="home-1.html#" data-toggle="dropdown">Contact</a>
                                    <div class="dropdown-menu">
                                       <a class="dropdown-item" href="contact_01.html">Contact 1</a>
                                       <a class="dropdown-item" href="contact_02.html">Contact 2</a>
                                       <a class="dropdown-item" href="contact_03.html">Contact 3</a>                     
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="sidenav_cat address_slide">
                              <h3>Address</h3>
                              <div class="sidenav_cat_inner">
                                 <span>70 Bowman St. <br>
                                 South Windsor, CT 06074 </span>
                              </div>
                           </div>
                        </div>
                     </nav>
                  </div>
               </div>
            </div>
         </header>
         <main>
 
            
