<!DOCTYPE html>
<html lang="en">
      <title><?=$meta->meta_title?></title>
	   <meta name="keyword" content="<?=$meta->meta_keywords?>">
   		 <meta name="description" content="<?=$meta->meta_description?>">
      <meta charset="utf-8">

	  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/fav-icon-new.png">
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/default.min.css<?php echo date(); ?>">
	  <meta name="author" content="www.redsystem.id" />
	  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	  <meta name="revisit-after" content="2 days"/>
	  <meta name="robots" content="index, follow"/>
	  <meta name="rating" content="General"/>
	  <meta name="author" content="http://redsystem.id/"/>
	  <meta name="MSSmartTagsPreventParsing" content="true"/>
	  <meta name="apple-mobile-web-app-capable" content="yes">
	  <meta name="reply-to" content="info@redsystem.id">
	  <meta http-equiv="expires" content="Mon, 28 Jul <?php echo date('Y')+1;?> 11:12:01 GMT">
	  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


   </head>
   <body >
      
            <div id="mainnavbar" class="container-fluid p-0 menu-2 "  >
               <div class=" fixed-top  container-fluid">
                  <div class="row menu-1-inner justify-content-between"  >
                     <div class="col-auto menu-1-logo">
                        <a class="navbar-brand" href="<?=site_url()?>">
                        <img class="nb-light" src="<?php echo base_url(); ?>assets/images/logo-2.png">
                        </a>
                     </div>
                     <div class="col-md-7 main_nav_outer">
                        <nav class="main_nav">
                           <div class="dropdown ">
							   <a href="<?php echo base_url(); ?>">
								   <button type="button" class="btn btn-primary">Home</button>
							   </a>
                           </div>
                           <div class="dropdown ">
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                              Program
                              </button>
                              <div class="dropdown-menu">
								  <?php
								  foreach ($menu_layanan as $key){
									  ?>

									  <a class="dropdown-item" href="<?=base_url().'program/'.$key->menu_url?>" title="<?=$key->title?>"><?=$key->title?></a>
									  <?php
								  }
								  ?>
                              </div>
                           </div>
                           <div class="dropdown ">
                              <a href="<?php echo base_url();?>event">
                                 <button type="button" class="btn btn-primary">Events</button>
                              </a>
                           </div>
                           <div class="dropdown ">
                              <button type="button" class="btn btn-primary dropdown-toggle" href="<?php echo base_url();?>" data-toggle="dropdown">
                              Blog & News 
                              </button>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="<?php echo base_url();?>blog">Blog List</a>
                                 <a class="dropdown-item" href="<?php echo base_url();?>news">News List</a>
                              </div>
                           </div>
                           <div class="dropdown ">
                              <a href="<?php echo base_url();?>location">
                                 <button type="button" class="btn btn-primary">Location</button>
                              </a>
                           </div>
                           <div class="dropdown ">
                              <a href="<?php echo base_url();?>contact">
                                 <button type="button" class="btn btn-primary">Contact</button>
                              </a>
                           </div>
                           
                        </nav>
                     </div>
                     <div class="col-auto nav-search">
                        <div class="login_but">

                         </div>
                        <button class="searchclick">
                          <span class="ti- ti-search  svg-search"></span>
                        </button>
                        <form class="nav-search-form">
                           <input class="form-control " type="search" placeholder="Search" aria-label="Search">
                           <button class=" btn btn-outline-success my-2 my-sm-0" type="submit"><i class="ti-search"></i></button>
                        </form>
                        <span class=" col-toggler-2">
                           <button class="navbar-toggler collapsed" type="button" onclick="openNav()" >
                              <div class="hamburger hamburger--spring js-hamburger">
                                 <div class="hamburger-box">
                                    <div class="hamburger-inner"></div>
                                 </div>
                              </div>
                           </button>
                        </span>
                     </div>
                  </div>
                  <div  id="mySidenav" class=" navbar-4 sidenav">
                     <nav class=" ">
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                        <div class=" ">
                           
                           <div class="sidenav_cat sc_first">
                              <h3>Social links</h3>
                              <ul class="tagnav">
                                 <li>
                                    <a href="https://www.facebook.com/kelasrenang.id" title="facebook kelas renang"><span class=" ti-facebook "></span></a>
                                 </li>
                                 <li>
                                    <a href="https://www.instagram.com/kelasrenang.id/" title="instagram kelas renang"><span class=" ti-instagram "></span></a>
                                 </li>

                              </ul>
                           </div>
                           <div class="sidenav_cat">
                              <h3>Menu</h3>
                              <ul class="">
                                 <li class="nav-item dropdown">
                                    <a class="nav-link" href="<?php echo base_url();?>" >Home</a>
                                 </li>
                                 <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Program</a>
                                    <div class="dropdown-menu">
										<?php
										foreach ($menu_layanan as $key){
											?>
											<a class="dropdown-item" href="<?=base_url().'program/'.$key->menu_url?>" title="<?=$key->title?>"><?=$key->title?></a>
											<?php
										}
										?>
                                    </div>
                                 </li>
                                 <li class="nav-item dropdown">
                                    <a class="nav-link" href="<?php echo base_url();?>event" >Events</a>
                                 </li>
                                 <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="<?php echo base_url();?>" data-toggle="dropdown">Blog & News</a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="<?php echo base_url();?>blog">Blog List</a>
                                        <a class="dropdown-item" href="<?php echo base_url();?>news">News List</a>
                                    </div>
                                 </li>
                                 <li class="nav-item dropdown">
                                    <a class="nav-link" href="<?php echo base_url();?>location" >Location</a>
                                 </li>
                                 <li class="nav-item dropdown">
                                    <a class="nav-link" href="<?php echo base_url();?>contact" >Contact</a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </nav>
                  </div>
               </div>
            </div>
         </header>
         <main>
