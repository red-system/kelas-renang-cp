<section id="h-charter" class="main-section h-charter-sec swip ">
	<div class="container-fluid">
		<div class="row m-0 ">
			<div class="thm-h sec-main-h">
				<h2 class="wow fadeInUp" data-wow-duration="1s" >Our Team </h2>
			</div>
			<div class="col-sm-12"  >
				<div class="owl-carousel owl-carousel-3 center-carousel owl-theme">
					<?php foreach ($list_team as $team) { ?>
					<div class="item">
						<a  href="<?php echo site_url('team#'.$this->main->slug($team->title))?>">
							<div class="main-tilt">
								<div>
									<img class="main-back" src="<?php echo $this->main->image_preview_url($team->thumbnail);?>" alt="<?php echo $team->title ?>">
								</div>
								<div   class="inner-tilt">
									<img class="inner-img" src="<?php echo $this->main->image_preview_url($team->thumbnail); ?>" alt="<?php echo $team->title ?>">
									<div class="slide-cont">
										<div>
											<h3><?php echo $team->title ?></h3>
											<span><?php echo $team->position ?></span>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
