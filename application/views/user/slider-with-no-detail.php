<div class="header-inner hi-single-yacht mb-0">
          <div class="single-yacht owl-carousel  owl-theme">
			  <?php
			  	foreach ($slider as $item){
			  		?>
					<div class="item">
						<img src="<?=site_url().'upload/images/'.$item->thumbnail?>" title="<?=$item->title?>" alt="<?=$item->thumbnail_alt?>">
					</div>
			  	<?php
				}
			  ?>
          </div>
	<div class="tagline" style="text-shadow: 2px 3px black;">
		<h1><?=$meta->title?></h1>
	</div>
         </div>
         
        </div> 
