<section id="h-our-history-2" class="half-section main-section h-our-history-3 swip">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-sm-6 half-left jumbotron" style="background: url('<?=site_url().'upload/images/'.$page_image->thumbnail?>') no-repeat center;
            background-size: cover;"
            data-paroller-factor="-0.3"
            data-paroller-factor-xs="0.2" >
            </div>
            <div class="col-sm-6 half-right">
            <div class="writing-sec my-paroller" data-paroller-factor="0.1" data-paroller-type="foreground" data-paroller-direction="horizontal">
                <div class="thm-h">
                    <h2 class="wow fadeInUp" data-wow-duration="0.7s"><?=$about_page->title?></h2>
                    <h4><?=$about_page->title_sub?></h4>
                </div>
                <p class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">
					<?=$about_page->short_description?>
                </p>
                <a href="<?=site_url()?>about-us" title="profile kelas renang" class="read-more button-fancy -color-1 wow fadeInUp" data-wow-duration="1s"><span class="btn-arrow"></span><span class="twp-read-more text">Continue Reading</span></a>
            </div>
            </div>
        </div>
    </div>
</section>
