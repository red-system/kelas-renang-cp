<section  class="y-section_inner h-our-history-2 ">
   <div class="container-fluid">
	  <div class="row m-0">
		 <div class="col-sm-12">
			<div class="thm-h text-center">
			   <h2 class="wow fadeInUp" data-wow-duration="0.7s">GALLERY</h2>
			</div>
			<div class="owl-carousel charter-carousel  owl-theme">
				<?php
				foreach ($gallery as $item){
					?>
					<div class="item gallery" data-dot="<?=$item->title?>">
						<a  href="<?=$this->main->image_preview_url($item->thumbnail)?>">
						<img src="<?=$this->main->resize($item->thumbnail,'315x150')?>" title="<?=$item->title?>" alt="<?=$item->thumbnail_alt?>">
						</a>
					</div>
					<?php
				}
				?>
			</div>
		 </div>
	  </div>
   </div>
</section>
