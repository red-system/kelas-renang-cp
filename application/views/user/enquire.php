<section  class="y-section_inner y-single_form">

      <div class="container-fluid">
          <div class="thm-h text-center">
             <h2 class="wow fadeInUp" data-wow-duration="0.7s">Mau daftar Kelas Renang?</h2>
          </div>
		  <form method="post" action="<?=site_url().'contact/send-message'?>" class="form-submit">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
					<input type="text" class="form-control" placeholder="First name" name="first_name"
						   required>
               </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
					<input type="text" class="form-control" placeholder="Last name" name="last_name"
						   required>
                 </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
					<input type="email" class="form-control" placeholder="Email" name="email" required>
                 </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
					<input type="text" class="form-control" placeholder="Kota" name="kota" required>
               </div>
              </div>
            </div>
			  <div class="row">
				  <div class="col-md-6">
					  <div class="form-group">
						  <input type="text" class="form-control" placeholder="Mobile Number (Whatsapp)"
								 name="whatsapp" required>
					  </div>
				  </div>
				  <div class="col-md-6">
					  <div class="form-group">
						  <select class="form-control" id="kelas_pilihan" name="services" required>
							  <option value="Select Class Type">Select Class Type</option>
							  <?php
							  foreach ($menu_layanan as $item){
								  ?>
								  <option value="<?=$item->title?>"><?=$item->title?></option>
								  <?php
							  }
							  ?>
						  </select>
					  </div>
				  </div>
			  </div>
			  <div class="row">
				  <div class="col-md-6">
					  <div class="form-group">
						  <?=$captcha?>
					  </div>
				  </div>
				  <div class="col-md-6">
					  <div class="form-group">
						  <input type="text" class="form-control" placeholder="Captcha"
								 name="captcha" required>
					  </div>
				  </div>
			  </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Message</label>
                  <textarea class="form-control" placeholder=""></textarea>
               </div>
              </div>
            </div>
             
             <button type="submit" class="btn btn-primary">Submit</button>
          </form>
      </div>
      </section>
