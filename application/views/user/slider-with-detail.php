        <div class="header-inner hi-single-yacht mb-0">
          <div class="single-yacht owl-carousel  owl-theme">
              <div class="item">
                 <img src="<?php echo base_url(); ?>assets/images/single_slider_02.jpg">
              </div>
               <div class="item">
                 <img src="<?php echo base_url(); ?>assets/images/single_slider_03.jpg">
              </div>
               <div class="item">
                 <img src="<?php echo base_url(); ?>assets/images/single_slider_04.jpg">
              </div>
               <div class="item">
                 <img src="<?php echo base_url(); ?>assets/images/single_slider_05.jpg">
              </div>
               <div class="item">
                 <img src="<?php echo base_url(); ?>assets/images/single_slider_06.jpg">
              </div>
               <div class="item">
                 <img src="<?php echo base_url(); ?>assets/images/single_slider_07.jpg">
              </div>
               <div class="item">
                 <img src="<?php echo base_url(); ?>assets/images/single_slider_08.jpg">
              </div>
               <div class="item">
                 <img src="<?php echo base_url(); ?>assets/images/single_slider_09.jpg">
              </div>
          </div>
          <div class="header-cont">
            <div class="wow fadeInDown single-yacht_dtails" data-wow-delay=".9s">
                  <h1 class="y-heading">Clclandes 43.4</h1> 
                  <div>
                      <p><span class="value">69.65m / 229ft</span></p>
                      <p>from<span class="value"> $450,000 </span></p>
                  </div> 
                  <ul class="single-yacht_number">
                    <li><span>34</span>GUESTS</li>
                    <li><span>17</span>CABINS</li>
                    <li><span>38</span>CREW</li>
                  </ul>
                  <ul>
                    <li><a href="sale_single.html#">+ Yach Details</a></li>
                    <li><a href="sale_single.html#">Video</a></li>
                    <li><a href="sale_single.html#">Brochure</a></li>
                  </ul>
                  <div>
                    <a class="btn btn-primary-ouline" href="sale_single.html#">enquiry</a>
                    <ul class="single_side_action">
                      <li><a href="sale_single.html#"><span class=" ti-heart "></span></a></li>
                      <li><a href="sale_single.html#"><span class="  ti-anchor "></span></a></li>
                      <li><a href="sale_single.html#"><span class=" ti-share "></span></a></li>
                      <li><a href="sale_single.html#"><span class=" ti-eye "></span></a></li>  
                    </ul>
                 </div>
            </div>

         </div>
         
        </div> 