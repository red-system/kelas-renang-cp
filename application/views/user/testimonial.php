<section class="main-section testimonial swip">
               <div class="container-fluid">
                  <div class="thm-h text-center">
                     <h2 class="wow fadeInUp" data-wow-duration="0.7s"><?=$testimonial_page->title?></h2>
                     <h4><?=$testimonial_page->title_sub?></h4>
                  </div>
                  <div class="testimonail_outer">
                     <div class="owl-carousel testimonail_slider owl-theme">

						 <?php foreach ($testimonial as $item)
						 {
							?>
							 <div class="item">
								 <div class="testimonail_box">
									 <div class="testimonail_text">
										 <p><?=$item->description?></p>
									 </div>
									 <div class="testimonail_user">
										 <img src="<?=$this->main->resize($item->thumbnail,'84x84')?>" alt="<?=$item->thumbnail_alt?>" title="<?=$item->title?>">
										 <span><em><?=$item->title?></em></span>
									 </div>
									 <div class="testimonial_rating">
										 <i class="fas fa-star"></i>
										 <i class="fas fa-star"></i>
										 <i class="fas fa-star"></i>
										 <i class="fas fa-star"></i>
										 <i class="fas fa-star"></i>
									 </div>
								 </div>
							 </div>
						 	<?php
						 }

						 ?>

                        </div>
                     </div>
                  </div>
               </div>
            </section>
