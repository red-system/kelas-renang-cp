<br><br>
<section id="y-single_info">
	<div class="y-single_info">
		<div class="container-fluid detail">
			<div class=" y-single_info_inner y-section_content">
				<div class="row m-0 justify-content-between">
					<?php foreach ($list_team as $key => $team) { ?>
						<?php if ($key % 2 == 0) { ?>
						<div class="col-lg-6" id="<?= $this->main->slug($team->title) ?>">
							<div class="img-team-detail">
								<img src="<?php echo $this->main->image_preview_url($team->thumbnail) ?>" alt="<?php echo $team->thumbnail_alt ? $team->thumbnail_alt : $team->title ?>">
							</div>
						</div>
						<div class="col-lg-6 profile-team">
							<div class="col-lg-12 mt-3">
								<p class="mb-0 profile-team-name"><?= $team->title; ?>, <?= $team->age?></span> </p>
								<p class="mb-2 profile-team-position"><?= $team->position; ?></span> </p>
								<?= $team->description; ?>
							</div>
						</div>
						<?php } else { ?>
						<div class="col-lg-6 profile-team" id="<?= $this->main->slug($team->title) ?>">
							<div class="col-lg-12 mt-3">
								<p class="mb-0 text-right profile-team-name"><?= $team->title; ?>, <?= $team->age?></span> </p>
								<p class="mb-2 text-right profile-team-position"><?= $team->position; ?></span> </p>
								<?= $team->description; ?>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="img-team-detail">
								<img src="<?php echo $this->main->image_preview_url($team->thumbnail) ?>" alt="<?php echo $team->thumbnail_alt ? $team->thumbnail_alt : $team->title ?>">
							</div>
						</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
