<section class="main-section our-services swip">
               <div class="container-fluid">
                  <div class="thm-h text-center">
                     <h2 class="wow fadeInUp" data-wow-duration="0.7s"><?=$service_page->title?></h2>
                     <h4><?=$service_page->title_sub?></h4>
                  </div>
                  <div class="row m-0">
                     <div class="col-md-3">
                        <div class="servicebox-first" style="background-image:url(<?php echo base_url();?>assets/images/kelas-renang-servis.jpg)">
                           <div class="thm-h text-center">
                              <h3 class="wow fadeInUp" data-wow-duration="0.7s" style="color:white;"><a href="https://www.kelasrenang.id" title="kelas renang"><i>kelasrenang.id</i></a></h3>
                              <h4>Program Khusus Sesuai Kebutuhan</h4>
                              <br>
                              <p>kami sangat mengutamakan kenyamanan pada saat belajar terutama untuk masyrakat yang mempunyai trauma ataupun fobia dengan air. </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-9">
                        <div class="row servicebox-row">
                           <?php
						   	foreach ($services as $item){
						   		?>
								<div class="col-md-4">
									<a href="<?=base_url().'program/'.strtolower($item->url_title)?>" class="text-black">
									<div class="servicebox text-justify">
										<img src="<?=$this->main->resize($item->thumbnail,'45x45')?>" title="<?=$item->title?>" alt="<?=$item->thumbnail_alt?>">
										<h4><?=$item->title?></h4>
										<p><?=$item->title_sub?></p>
									</div>
									</a>
								</div>
								<?php
							}
						   ?>

                        </div>
                     </div>
                  </div>
               </div>
            </section>
