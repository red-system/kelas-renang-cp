<?php foreach($list as $item): ?>
    <br><br>
    <section id="y-single_info">
        <div class="y-single_info">
            <div class="container-fluid detail">
                <div class=" y-single_info_inner y-section_content">
                    <div class="row m-0 justify-content-between">
                        <h2 class="text-center font-weight-bold w-100"><?= $item->title ?></h2>
                        <br>
                        <img class="w-100 mb-2" src="<?= base_url('upload/images/' . $item->thumbnail) ?>" alt="<?= $item->thumbnail_alt ?>">
                        <br><br>
                        <?=$item->description?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endforeach ?>