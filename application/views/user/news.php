<br><br>
<section id="y-single_info">
          <div class="y-single_info">
            <div class="container-fluid">
                <div class="y-single_info_inner y-section_content row m-0">
                    
                     
                      <div class="y-product_listing_side col-sm-12">
                        <div class="y-dest_list ">
							<?php
								$class = array("","pull-right");
								$classBottom = array("pull-right","");
								$i = 1;
								foreach ($list as $item){
									?>
									<div class="y-dest_list_single row">
										<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 <?=$class[$i%2]?> wow fadeInLeft" data-wow-duration="1s">
											<a href="<?=$item->menu_url?>" title="<?=$item->title?>"><img src="<?=site_url().'upload/images/'.$item->thumbnail?>" title="<?=$item->title?>" class="img-fluid" alt="<?=$item->thumbnail_alt?>"></a>
										</div>
										<div class="col-sm-7 <?=$classBottom[$i%2]?> wow fadeInRight" data-wow-duration="1s">
											<div class="thm-h">
												<h3><a href="<?=$item->menu_url?>" title="<?=$item->title?>"><?=$item->title?></a></h3>
											</div>
											<div>
												<?=$item->short_description?>
											</div>
											<a href="<?=$item->menu_url?>" title="<?=$item->title?>" class="read-more button-fancy -color-1 ">
												<span class="btn-arrow"></span>
												<span class="twp-read-more text">Continue Reading</span>
											</a>
										</div>
									</div>
									<?php
									$i++;
								}
							?>

                        </div> 
                      </div> 
                    
                    <div class="y-pagination row clearfix">
                       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                         <ul class="row y-pagination_num clearfix">
                          <?=$paging?>
                         </ul>
                       </div>  
                    </div>
                </div>
            </div>
          </div>
        </section> 
