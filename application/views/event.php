<?php for ($x = 0; $x <= 10; $x++) {?><section id="h-our-history-2" class="half-section main-section h-our-history-3   swip">
               <div class="container-fluid">
                  <div class="row ">
                     <div class="col-sm-6 half-left jumbotron" style="background: url('assets/images/our-history-2.jpg') no-repeat center;
                        background-size: cover;"
                        data-paroller-factor="-0.1"
                        data-paroller-factor-xs="0.1" >
                     </div>
                     <div class="col-sm-6 half-right">
                        <div class="writing-sec my-paroller" data-paroller-factor="0.1" data-paroller-type="foreground" data-paroller-direction="horizontal">
                           <div class="thm-h thm-h-3">
                              <h2 class=" thm-h-3-big" data-paroller-factor="-0.05" data-paroller-type="foreground" data-paroller-direction="vertical">journey</h2>
                              <h4>Our history</h4>
                           </div>
                           <p class="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">Owning a superyacht is the freedom to explore the world on your terms without limits. With 45 years of experience sourcing the best yachts at the best prices, YPI’s team of worldwide brokers are experts in finding the right yacht for every client. 
                           </p>
                           <a href="home-3.html#" class="read-more button-fancy -color-1 wow fadeInUp" data-wow-duration="1s"><span class="btn-arrow"></span><span class="twp-read-more text">Continue Reading</span></a>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
<?php }?>