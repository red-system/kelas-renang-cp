<section  id="h-section-menu" class="swip">
	<form method="post" action="<?=site_url().'contact/send-message'?>" class="form-submit">
               <div class="container-fluid section-menu ">
                  <div class="row">

                     <div class="col-md-6 home-about wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="thm-h text-center">
                           <h2 class="wow fadeInUp" data-wow-duration="0.7s"><?=$meta->title?></h2>

                        </div>
                        <?=$meta->description?>
                     </div>
                     <div class="col-md-6  wow fadeInUp">
                        <div class="home-booknow">
                           <div class="booknow_box">
                              <h4>Book now</h4>
                              <form>
								  <div class="form-group">
									  <input type="text" class="form-control" placeholder="First name" name="first_name"
											 required>
								  </div>
								  <div class="form-group">
									  <input type="text" class="form-control" placeholder="Last name" name="last_name"
											 required>
								  </div>
								  <div class="form-group">
									  <input type="email" class="form-control" placeholder="Email" name="email" required>
								  </div>
								  <div class="form-group">
									  <input type="text" class="form-control" placeholder="Kota" name="kota" required>
								  </div>
								  <div class="form-group">
									  <input type="text" class="form-control" placeholder="Mobile Number (Whatsapp)"
											 name="whatsapp" required>
								  </div>
								  <div class="form-group">
									  <select class="form-control" id="kelas_pilihan" name="services" required>
										  <option value="Select Class Type">Select Class Type</option>
										  <?php
										  	foreach ($menu_layanan as $item){
										  		?>
												<option value="<?=$item->title?>"><?=$item->title?></option>
										  		<?php
											}
										  ?>
									  </select>
								  </div>

								  <div class="form-group">
									<textarea class="form-control" placeholder="Message" name="message"
											  required></textarea>
								  </div>
								  <div class="form-group">
									  <?=$captcha?>
								  </div>
								  <div class="form-group">
									  <input type="text" class="form-control" placeholder="Captcha"
											 name="captcha" required>
								  </div>
                                 <button type="submit" class="btn btn-primary">Submit</button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
	</form>
            </section>
