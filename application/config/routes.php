<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$connection = mysqli_connect('159.65.132.116', 'danta', 'anY73SvgB3IPkHIy', 'kelas_renang');
//$connection = mysqli_connect('localhost', 'danta', 'anY73SvgB3IPkHIy', 'kelas_renang');

$route['default_controller'] = 'home';


$route['proweb'] = 'proweb/login';
$route['proweb/login'] = 'proweb/login/process';

$route['event'] = 'event';
$route['about-us'] = 'about';
$route['event/:num'] = 'event';

$route['blog'] = 'blog';
$route['image-display/:any/:any'] = 'imagedisplay';
$route['blog/:num'] = 'blog';

$route['team'] = 'team';

$route['location'] = 'location';
$route['location/:num'] = 'location';

$route['contact/send-message'] = 'contact/send_message';


$route['404_override'] = 'PageNotFound';

$route['translate_uri_dashes'] = FALSE;
$services = mysqli_query($connection, "SELECT id, title FROM layanan where category = 'layanan'");
while ($data = mysqli_fetch_array($services)) {
	$route['program/' . slug($data['title'])] = "program/item/".$data['id'];

}

$services = mysqli_query($connection, "SELECT id, title FROM event where category = 'event'");
while ($data = mysqli_fetch_array($services)) {
	$route['event/' . slug($data['title'])] = "event/item/".$data['id'];

}

$services = mysqli_query($connection, "SELECT id, title FROM blog ");
while ($data = mysqli_fetch_array($services)) {
	$route['blog/' . slug($data['title'])] = "blog/item/".$data['id'];

}

$services = mysqli_query($connection, "SELECT id, title FROM news ");
while ($data = mysqli_fetch_array($services)) {
	$route['news/' . slug($data['title'])] = "news/item/".$data['id'];

}

function slug($string)
{
    $find = array(' ','’',':', '/', '&', '\\', '\'', ',','(',')');
    $replace = array('-','-','-', '-', 'and', '-', '-', '-','','');

    $slug = str_replace($find, $replace, strtolower($string));

    return $slug;
}
