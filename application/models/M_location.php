<?php

class M_location extends CI_Model
{

	private $table = 'location';

	public function get_data()
	{
		// $this->db->where('category','event');
		return $this->db->get($this->table);
	}

	public function get_where($where)
	{
		return $this->db->where($where)->get($this->table);
	}

	public function input_data($data)
	{
		$this->db->insert($this->table, $data);
	}

	public function delete_data($where)
	{
		$this->db->where($where);
		$this->db->delete($this->table);
	}

	function update_data($where, $data)
	{
		$this->db->where($where);
		$this->db->update($this->table, $data);
	}
}
