<?php

class M_blog extends CI_Model
{

	protected $table = 'blog';

	public function get_data()
	{
		return $this->db->get($this->table);
	}

	public function input_data($data)
	{
		$this->db->insert($this->table, $data);
	}

	public function row_data($where) {
		return $this->db->where($where)->get($this->table)->row();
	}

	public function delete_data($where)
	{
		$this->db->where($where);
		$this->db->delete($this->table);
	}

	function update_data($where, $data)
	{
		$this->db->where($where);
		$this->db->update($this->table, $data);
	}
	function home_latest_news(){
        $this->db->select('blog.*,team.title as author,blog_category.title as category');
        $this->db->join('team', 'team.id = blog.id_team');
        $this->db->join('blog_category', 'blog_category.id = blog.id_blog_category');
	    $this->db->where('blog.use', 'yes');
        $this->db->order_by('created_at', 'desc');
        return $this->db->get('blog', 3, 0)->result();
    }
    function total_publish(){
        $this->db->join('team', 'team.id = blog.id_team');
        $this->db->join('blog_category', 'blog_category.id = blog.id_blog_category');
        return $this->db->get('blog')->num_rows();
    }
    function list_publish($length,$start){
        $this->db->select('blog.*,team.title as author,blog_category.title as category');
        $this->db->join('team', 'team.id = blog.id_team');
        $this->db->join('blog_category', 'blog_category.id = blog.id_blog_category');
        $this->db->where('blog.use', 'yes');
        $this->db->order_by('created_at', 'desc');
        return $this->db->get('blog', $length, $start)->result();
    }
    function detail_blog($id){
        $this->db->select('blog.*,team.title as author,blog_category.title as category');
        $this->db->join('team', 'team.id = blog.id_team');
        $this->db->join('blog_category', 'blog_category.id = blog.id_blog_category');
        $this->db->where('blog.use', 'yes');
        $this->db->where('blog.id', $id);
        $this->db->order_by('created_at', 'desc');
        return $this->db->get('blog')->row();
    }
}
