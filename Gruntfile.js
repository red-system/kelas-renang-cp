module.exports = function(grunt) {

  var default_js = [
	  "assets/js/jquery.min.js",
	  "assets/js/popper.min.js",
	  "assets/js/bootstrap.min.js",
	  "assets/js/owl.carousel.min.js",
	  "assets/js/jquery.mousewheel.min.js",
	  "assets/js/magicscroll.js",
	  "assets/js/wow.min.js",
	  "assets/js/jquery.paroller.min.js",
	  "assets/js/custom_new.js",
	  "assets/js/tilt.jquery.min.js",
	  "assets/js/simple-lightbox.min.js",
	  "assets/js/app.js",
  ];

  var default_css = [
	  "assets/css/material.css",
	  "assets/css/reguler.css",
	  "assets/css/bootstrap.min.css",
	  "assets/css/themify-icons.css",
	  "assets/css/all.min.css",
	  "assets/css/animate.css",
	  "assets/css/owl.carousel.min.css",
	  "assets/css/magicscroll.css",
	  "assets/css/simple-lightbox.min.css",
	  "assets/css/style.css",
  ];



  grunt.initConfig({
    jsDistDir: 'assets/js/',
    cssDistDir: 'assets/css/',
    pkg: grunt.file.readJSON('package.json'),
    concat: {
            default_js: {
                options: {
                    separator: ';'
                },
                src: default_js,
                dest: '<%=jsDistDir%>default.js'
            },
            default_css: {
                src: default_css,
                dest: '<%=cssDistDir%>default.css'
            },

        },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          '<%=jsDistDir%>default.min.js': ['<%= concat.default_js.dest %>'],
        }
      }
    },
    cssmin: {
      add_banner: {
        options: {
          banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
        },
        files: {
          '<%=cssDistDir%>default.min.css': ['<%= concat.default_css.dest %>'],
        }
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', [
      'concat',
      'uglify',
      'cssmin'
  ]);

};
